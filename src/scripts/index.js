/** import del file scss sass per la compilazione */
import '../styles/index.scss';

/** import classi javascript, 
 * da node_modules o da classi locali
 * omettendo l'estensione del file
 */
import Quizzando from './Quizzando';
import Dom7 from 'dom7';

/** assegnamo all'oggetto window le istanze 
 * utili all'utilizzo da console
 * e facciamo in modo che il compilatore
 * le renda disponibili a tutte le classi javascript
 * utilizzate
 */
window.$ = Dom7;
window.quizzando = new Quizzando();

