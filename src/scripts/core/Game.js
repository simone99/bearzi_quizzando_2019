import Model from "../model/Model";
import Signal from "signals";

export default class Game {
    constructor() {
        /** questo controllo fa in modo che la classe
         * sia un Singleton, cioè che ha sempre solo un'istanza 
         * anche se viene fatto new in più parti del codice
         */
        if (!Game.instance) {
            // definiamo i Signal
            this.onQuizStart = new Signal();
            this.onRisposta = new Signal();
            this.onQuizTerminato = new Signal();
            // definiamo le proprietà d'istanza
            this._model = new Model();
            this._domande = null;
            this._classifica = null;
            this._profilo = null;

            this._categoria_vo = null;
            this.classifica_vo = null;
            this._profilo_vo = null;

            //
            Game.instance = this;
        }
        return Game.instance;
    }

    setCategoria(categoria_vo) {
        this._categoria_vo = categoria_vo;
        //
        this._model.onDomande.add(this._onDomandeResult, this);
        this._model.getDomande(this._categoria_vo.id);
    }

    _onDomandeResult(signal_object) {
        console.log(signal_object);
        this._domande = signal_object.data;
        //
        this.onQuizStart.dispatch({type: "onQuizStart"});
    }

    getDomanda(index_domanda) {
        var domanda_vo = this._domande[index_domanda];
        domanda_vo = JSON.parse(JSON.stringify(domanda_vo));
        console.log(domanda_vo);
        return domanda_vo;
    }

    getCategoria() {
        return JSON.parse(JSON.stringify(this._categoria_vo));
    }

    getClassifica(){
        return JSON.parse(JSON.stringify(this._classifica_vo));
    }

    getProfilo(){
        return JSON.parse(JSON.stringify(this._profilo_vo));
    }

    saveDomanda(domanda_vo) {
        for (var i=0;i<this._domande.length;i++) {
            var tmp_domanda_vo = this._domande[i];
            if (tmp_domanda_vo.id==domanda_vo.id) {
                this._domande[i] = domanda_vo;
                console.log("domanda: "+domanda_vo.domanda);
                console.log("i="+i+"length "+(this._domande.length-1));
                if (i==this._domande.length-1) {
                   this._model.onRisultato.add(this._onRisultatoResult,this);
                   this._model.risultatoQuiz(this._domande);
                } else {
                    this.onRisposta.dispatch({
                        type: "onRisposta",
                        data: {
                            index_domanda: i
                        }
                    });
                }
                break;
            }
        }
    }
    
    _onRisultatoResult(signal_object){
        this.risultato = signal_object.data;
        console.log(this.risultato);
        this.onQuizTerminato.dispatch({
            type: "onQuizTerminato",
        });
    }
}