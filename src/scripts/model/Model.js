import Signal from "signals";

export default class Model {
    constructor() {
        /** questo controllo fa in modo che la classe
         * sia un Singleton, cioè che ha sempre solo un'istanza 
         * anche se viene fatto new in più parti del codice
         */
        if (!Model.instance) {
            // definiamo i Signal del modello
            this.onUtente = new Signal();
            this.onLogin = new Signal();
            this.onCategorie = new Signal();
            this.onDomande = new Signal();
            this.onRisultato = new Signal();
            this.onErroreConnessione = new Signal();
            this.onClassifica = new Signal();
            this.onProfilo = new Signal();
            // definiamo le proprietà d'istanza
            this.utente = null;
            //
            Model.instance = this;
        }
        return Model.instance;
    }

    getUtente() {
        // TODO: manca chiamata al server
        this.onUtente.dispatch({
            type: "onUtente",
            data: this.utente
        });
    }

    login(username, password) {
        Framework7.request({
            url: 'https://gesco.bearzi.it/api/login_check',
            data: {
                username: username,
                password: password
            },
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            crossDomain: true,
            success: (response) => {
                console.log("success", response);
                var token = response.token;
                this.utente = {
                    token: token
                };
                this.onLogin.dispatch({
                    type: "onLogin",
                    data: this.utente
                });
                this.onUtente.dispatch({
                    type: "onUtente",
                    data: this.utente
                });
            },
            error: () => {
                console.log("error");
                this.onErroreConnessione.dispatch({
                    type: "onErroreConnessione"
                });
            }
        });
        /*
        this.utente = {
            username: "utente1",
            avatar: "http://www.asdf.it/a.jpg",
            punteggio: 150,
            livello: 1,
            posizione: 5
        };
        this.onLogin.dispatch({
            type: "onLogin",
            data: this.utente
        });
        this.onUtente.dispatch({
            type: "onUtente", 
            data: this.utente
        });
        */
    }

    getCategorie() {
        // TODO: manca la chiamata al server
        Framework7.request.json('public/temp/categorie.json', (data) => 
            {
                console.log(data);
                this.onCategorie.dispatch({
                    type: "onCategorie",
                    data: data
                });
            }
        );
    }

    getDomande(id_categoria) {
        // TODO: manca la chiamata al server
        Framework7.request.json('public/temp/domande.json', (data) => 
            {
                console.log(data);
                this.onDomande.dispatch({
                    type: "onDomande",
                    data: data
                });
            }
        );
    }
    
    risultatoQuiz(domande) {
        // TODO: manca la chiamata al server
        Framework7.request.json('public/temp/risultato.json', (data) => 
            {
                console.log(data);
                this.onRisultato.dispatch({
                    type: "onRisultato",
                    data: data
                });
            }
        );  
    }

    getClassifica(){
        Framework7.request.json('public/temp/classifica.json', (data) => {
            console.log(data);
            this.onClassifica.dispatch({
                type: "onClassifica",
                data:data
            });
        });

    }

    getProfilo(){
        Framework7.request.json('public/temp/profilo.json', (data) => {
            console.log(data);
            this.onProfilo.dispatch({
                type: "onProfilo",
                data:data
            });
        });

    }
}