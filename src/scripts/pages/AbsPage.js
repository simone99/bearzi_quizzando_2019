import Model from "../model/Model";
import Game from "../core/Game";

/**
 * classe astratta, non viene utilizzata con in comando new
 * ma viene estesa da altre classi
 * contiene le funzioni condivise da tutte le pagine
 */
export default class AbsPage {
    constructor(pagedata) {
        this._pagedata = pagedata;
        this._model = new Model();
        this._game = new Game();
        this._init();
    }
    _render() {

    }
    _init() {
        console.log("init");
    }
    destroy() {
        console.log("destroy");
        delete this._pagedata;
        delete this._model;
        delete this._game;
    }
}