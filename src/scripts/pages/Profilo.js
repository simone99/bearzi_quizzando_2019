import AbsPage from "./AbsPage";

/**
 * le classi di pagina estendono AbsPage
 */
export default class Profilo extends AbsPage {
    constructor(pagedata) {
        console.log("Profilo");
        /** richiamiamo super per ultima per avere
         * tutte le variabili di istanza disponibili
         * nel metodo init
         */
        //
        super(pagedata);
        //
        this._profilo = null;
    }

    _init() {
        super._init();
        //
        this._model.onProfilo.add(this._onProfiloResult,this);
        this._model.getProfilo();
    }

    _onProfiloResult(signal_object) {
        console.log(signal_object);
        var contenitore_lista = this._pagedata.$el.find(".list.profilo ul");
        console.log(contenitore_lista.html());
        var template = this._pagedata.$el.find("#tmpl-profilo").html();
        var html = "";
        this._profilo = signal_object.data;
        for (var i=0;i<signal_object.data.length;i++) {
            var temp_html = template;
            var profilo_vo = signal_object.data[i];
            temp_html = temp_html.replace("{icon}",profilo_vo.icon);
            temp_html = temp_html.replace("{utente}",profilo_vo.utente);
            temp_html = temp_html.replace("{punteggio}",profilo_vo.punteggio);
            temp_html = temp_html.replace("{posizione}",profilo_vo.posizione);
            html += temp_html;
        }
        contenitore_lista.append(html);
        contenitore_lista.on("click",(evt) => {
            this._clickHandler(evt);
        });
    }

    _clickHandler(event) {
        var target = event.target;
        var clicked = $(target).closest("a");
        var id_profilo = clicked.attr("data-id");
        console.log(id_profilo);
        var profilo_vo;
        for (var i = 0; i < this._profilo.length; i++) {
            if (this._profilo[i].id==id_profilo) {
                profilo_vo = this._profilo[i];
            }
        }
    }

    destroy() {
        this._model.onProfilo.remove(this._onProfiloResult,this);
        var contenitore_lista = this._pagedata.$el.find(".list.profilo ul");
        contenitore_lista.off("click");
        //
        delete this._profilo;
        //
        super.destroy();
    }
}