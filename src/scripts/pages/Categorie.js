import AbsPage from "./AbsPage";

/**
 * le classi di pagina estendono AbsPage
 */
export default class Categorie extends AbsPage {
    constructor(pagedata) {
        console.log("Categorie");
        /** richiamiamo super per ultima per avere
         * tutte le variabili di istanza disponibili
         * nel metodo init
         */
        //
        super(pagedata);
        //
        this._categorie = null;
    }

    _init() {
        super._init();
        //
        this._model.onCategorie.add(this._onCategorieResult,this);
        this._model.getCategorie();
    }

    _onCategorieResult(signal_object) {
        console.log(signal_object);
        var contenitore_lista = this._pagedata.$el.find(".list.categorie ul");
        console.log(contenitore_lista.html());
        var template = this._pagedata.$el.find("#tmpl-categoria").html();
        var html = "";
        this._categorie = signal_object.data;
        for (var i=0;i<signal_object.data.length;i++) {
            var temp_html = template;
            var categoria_vo = signal_object.data[i];
            temp_html = temp_html.replace("{id}",categoria_vo.id);
            temp_html = temp_html.replace("{icon}",categoria_vo.icon);
            temp_html = temp_html.replace("{nome}",categoria_vo.nome);
            html += temp_html;
        }
        contenitore_lista.append(html);
        contenitore_lista.on("click",(evt) => {
            this._clickHandler(evt);
        });
    }

    _clickHandler(event) {
        var target = event.target;
        var clicked = $(target).closest("a");
        var id_categoria = clicked.attr("data-id");
        console.log(id_categoria);
        var categoria_vo;
        for (var i = 0; i < this._categorie.length; i++) {
            if (this._categorie[i].id==id_categoria) {
                categoria_vo = this._categorie[i];
            }
        }
        this._game.setCategoria(categoria_vo);
    }

    destroy() {
        this._model.onCategorie.remove(this._onCategorieResult,this);
        var contenitore_lista = this._pagedata.$el.find(".list.categorie ul");
        contenitore_lista.off("click");
        //
        delete this._categorie;
        //
        super.destroy();
    }
}