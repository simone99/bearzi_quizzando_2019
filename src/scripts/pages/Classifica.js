import AbsPage from "./AbsPage";

/**
 * le classi di pagina estendono AbsPage
 */
export default class Classifica extends AbsPage {
    constructor(pagedata) {
        console.log("Classifica");
        /** richiamiamo super per ultima per avere
         * tutte le variabili di istanza disponibili
         * nel metodo init
         */
        //
        super(pagedata);
        //
        this._classifica = null;
    }

    _init() {
        super._init();
        //
        this._model.onClassifica.add(this._onClassificaResult,this);
        this._model.getClassifica();
    }

    _onClassificaResult(signal_object) {
        console.log(signal_object);
        var contenitore_lista = this._pagedata.$el.find(".list.classifica ul");
        console.log(contenitore_lista.html());
        var template = this._pagedata.$el.find("#tmpl-classifica").html();
        var html = "";
        this._classifica = signal_object.data;
        for (var i=0;i<signal_object.data.length;i++) {
            var temp_html = template;
            var classifica_vo = signal_object.data[i];
            temp_html = temp_html.replace("{id}",classifica_vo.id);
            temp_html = temp_html.replace("{posizione}",classifica_vo.posizione);
            temp_html = temp_html.replace("{icon}",classifica_vo.icon);
            temp_html = temp_html.replace("{utente}",classifica_vo.utente);
            temp_html = temp_html.replace("{punteggio}",classifica_vo.punteggio);
            html += temp_html;
        }
        contenitore_lista.append(html);
        /*da commentare (serve per verificare se funziona l'id dell'utente)*/
        contenitore_lista.on("click",(evt) => {
            this._clickHandler(evt);
        });
    }

    /*da commentare (metodo che viene passato al contentiore_lista per verificare se funziona l'id dell'utente)*/
    _clickHandler(event) {
        var target = event.target;
        var clicked = $(target).closest("a");
        var id_classifica = clicked.attr("data-id");
        console.log(id_classifica);
        var classifica_vo;
        for (var i = 0; i < this._classifica.length; i++) {
            if (this._classifica[i].id==id_classifica) {
                classifica_vo = this._classifica[i];
            }
        }
        
    }

    destroy() {
        this._model.onClassifica.remove(this._onClassificaResult,this);
        var contenitore_lista = this._pagedata.$el.find(".list.classifica ul");
        contenitore_lista.off("click");
        //
        delete this._classifica;
        //
        super.destroy();
    }
}