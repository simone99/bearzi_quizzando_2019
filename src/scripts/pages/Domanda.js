import AbsPage from "./AbsPage";

/**
 * le classi di pagina estendono AbsPage
 */
export default class Domanda extends AbsPage {
    constructor(pagedata) {
        console.log("Domanda");
        /** richiamiamo super per ultima per avere
         * tutte le variabili di istanza disponibili
         * nel metodo init
         */
        //
        super(pagedata);
        //
        this._domanda_vo;
        this._risposta_vo ;
        this._toastTop;

    }

    _init() {
        super._init();
        //
        console.log(this._pagedata.route.params.index_domanda);
        var indice_domanda = parseInt(this._pagedata.route.params.index_domanda);
        this._domanda_vo = this._game.getDomanda(indice_domanda);
        console.log(this._domanda_vo);
        //

        //
        this._render();
        this._pagedata.$el.find("a.button").on("click", (evt) => this._clickHandler(evt));
        this._pagedata.$el.find("i.icon").on("click", (evt) => this._clickHandler2(evt));
        this._pagedata.$el.find("label.item-radio.item-content").on("click", (evt) => this._clickHandler2(evt));
        
    }

    _clickHandler(evt) {
        console.log("click avanti");
        var app = this._pagedata.app;
        var form = this._pagedata.$el.find("form");
        var formdata = app.form.convertToData(form[0]);
        var id_risposta = formdata.id_risposta;
        if (id_risposta) {
            // TODO: salvo la risposta nell'istanza di Game
            console.log("id risposta "+id_risposta);
            console.log(this);
            console.log(this._domanda_vo);
            for (var i=0; i<this._domanda_vo.risposte.length; i++) {
                var risposta_vo = this._domanda_vo.risposte[i];
                if (risposta_vo.id==id_risposta) {
                    risposta_vo.rispostaUtente = true;
                }
            }
            this._game.saveDomanda(this._domanda_vo);
        } else {
            // TODO: avviso l'utente che deve scegliere una risposta
            // Create toast
            if (!this._toastTop) {
                this._toastTop = app.toast.create({
                text: 'Scegliere almeno una risposta',
                position: 'top',
                closeTimeout: 2000,
              });
            }
            // Open it
            this._toastTop.open();
        }

    }

    _clickHandler2(evt) {
        console.log("click risposta");
        var app = this._pagedata.app;
        var form = this._pagedata.$el.find("form");
        var formdata = app.form.convertToData(form[0]);
        var id_risposta = formdata.id_risposta;
        var array = [];
        if (id_risposta){
            // TODO: salvo la risposta nell'istanza di Game
            console.log("id risposta "+id_risposta);
            console.log(this);
            console.log(this._domanda_vo);
            for (var i=0; i<this._domanda_vo.risposte.length; i++) {
                var risposta_vo = this._domanda_vo.risposte[i];
                if (risposta_vo.id==id_risposta) {
                    risposta_vo.rispostaUtente = true; 
                }
            }
        
        }
    }

    _render() {
        var p_titolo = this._pagedata.$el.find(".testo-domanda p");
        p_titolo.html(this._domanda_vo.domanda);
        //
        var contenitore_risposte = this._pagedata.$el.find(".list.risposte ul");
        var template = this._pagedata.$el.find("#tmpl-risposta").html();
        console.log(template);
        var html = "";
        for (var i = 0; i < this._domanda_vo.risposte.length; i++) {
            var risposta_vo = this._domanda_vo.risposte[i];
            var tmp_html = template;
            tmp_html = tmp_html.replace("{id_risposta}", risposta_vo.id);
            tmp_html = tmp_html.replace("{risposta}", risposta_vo.testo);
            html += tmp_html;
        }
        contenitore_risposte.append(html);
        //
        var categoria_vo = this._game.getCategoria();
        this._pagedata.$el.find(".title").html(categoria_vo.nome);
        //
        super._render();
    }

    destroy() {
        this._pagedata.$el.find("a.button").off("click");
        this._pagedata.$el.find("i.icon").off("click");
        this._pagedata.$el.find("label.item-radio.item-content").off("click");
        delete this._domanda_vo;
        //
        if (this._toastTop) {
            this._toastTop.destroy();
        }
        delete this._toastTop;
        //
        super.destroy();
    }
}

