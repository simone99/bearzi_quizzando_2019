import AbsPage from "./AbsPage";

/**
 * le classi di pagina estendono AbsPage
 */
export default class Home extends AbsPage {
    constructor(pagedata) {
        console.log("Home");
        /** richiamiamo super per ultima per avere
         * tutte le variabili di istanza disponibili
         * nel metodo init
         */
        super(pagedata);
    }

    _init() {
        super._init();
    }
}